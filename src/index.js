// application code

import {initializeApp} from 'firebase/app'

// collection returns a collRef for the named collection
// onSnapshot(colref, action(snapshot)) calls action if colref changes
// doc returns a refrence to a document within a collection
// query(colRef, where(query-strings), orderBy(field)) 
import {getFirestore, collection, onSnapshot,
	addDoc, deleteDoc, doc,
	query, where,
	orderBy, serverTimestamp,
	getDoc, updateDoc,
       } from 'firebase/firestore'

import {
    getAuth,
    createUserWithEmailAndPassword,
    signInWithEmailAndPassword, signOut,
    onAuthStateChanged,
} from 'firebase/auth'

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyAz_64V9OB57FxLRXVLlwO6vf5jVbqMG5E",
  authDomain: "milindbkamble-publicblog-f5d7d.firebaseapp.com",
  projectId: "milindbkamble-publicblog-f5d7d",
  storageBucket: "milindbkamble-publicblog-f5d7d.appspot.com",
  messagingSenderId: "852723274263",
  appId: "1:852723274263:web:f0156ece73cf1a066fc252",
  measurementId: "G-TLJ48W3210"
}

// init firebase
initializeApp(firebaseConfig)

// init services
const db = getFirestore()
const auth = getAuth()

const colRef = collection(db, 'books')

// queries
// create a query
const q = query(colRef, where("author", "==", "Shivaji Sawant"), orderBy('createdAt'))

// if query subset changes, display the subset filtered by the query
// return value of onSnapshot() is a method that can be invoked to remove monitoring for that change
// here onSnapshot is monitoring change to a subset of documents based on author
const unsubCol = onSnapshot(
    q,
    (snapshot) => {
	let books = []
	snapshot.docs.forEach(doc => {
	    books.push({ ...doc.data(), id: doc.id })
	})
	console.log(books)
    },
    (error) => {
	console.log("onSnapshot for query, error = %s", error.message)
    }
)

// adding docs. An submit event listener for the Add document form
// title and author are provided by user through the form fields
// connect to backend, get collRef and call addDoc()
const addBookForm = document.querySelector('.add')
addBookForm.addEventListener('submit', (e) => {
    e.preventDefault()

    addDoc(colRef, {
	title: addBookForm.title.value,
	author: addBookForm.author.value,
	createdAt: serverTimestamp()
    })
	.then(() => {
	    addBookForm.reset()
	})
	.catch(err => {
	    console.log(err.message)
	})
})

// deleting docs. An submit event listener for Deletion of document with known ID
// get docRef and delete it
const deleteBookForm = document.querySelector('.delete')
deleteBookForm.addEventListener('submit', (e) => {
    e.preventDefault()

    const docRef = doc(db, 'books', deleteBookForm.id.value)

    deleteDoc(docRef)
	.then(() => {
	    deleteBookForm.reset()
	})
	.catch(err => {
	    console.log(err.message)
	})
})

// fetching a single document (& realtime)
const docRef = doc(db, 'books', 'DAW3hbI3ZOVzsXEMSENv')

// getDoc(docRef)
//   .then(doc => {
//     console.log(doc.data(), doc.id)
//   })

// return value of onSnapshot() is a method that can be invoked to remove monitoring for that change
// here onSnapshot is monitoring change to a specific document
const unsubDoc = onSnapshot(
    docRef,
    (doc) => {
	console.log(doc.data(), doc.id)
    },
    (error) => {
	console.log("onSnapshot for document error: %s", error.message)
    }
)

// updating a document
const updateForm = document.querySelector('.update')
updateForm.addEventListener('submit', (e) => {
    e.preventDefault()

    let docRef = doc(db, 'books', updateForm.id.value)

    updateDoc(docRef, {
	title: 'updated title'
    })
	.then(() => {
	    updateForm.reset()
	})
	.catch(err => {
	    console.log(err.message)
	})
})

// signing users up
const signupForm = document.querySelector('.signup')
signupForm.addEventListener('submit', (e) => {
  e.preventDefault()

  const email = signupForm.email.value
  const password = signupForm.password.value

  createUserWithEmailAndPassword(auth, email, password)
    .then(cred => {
      // console.log('user created:', cred.user)
      signupForm.reset()
    })
    .catch(err => {
      console.log(err.message)
    })
})

// logging in and out
const logoutButton = document.querySelector('.logout')
logoutButton.addEventListener('click', () => {
  signOut(auth)
    .then(() => {
      // console.log('user signed out')
    })
    .catch(err => {
      console.log(err.message)
    })
})

const loginForm = document.querySelector('.login')
loginForm.addEventListener('submit', (e) => {
  e.preventDefault()

  const email = loginForm.email.value
  const password = loginForm.password.value

  signInWithEmailAndPassword(auth, email, password)
    .then(cred => {
      // console.log('user logged in:', cred.user)
      loginForm.reset()
    })
    .catch(err => {
      console.log(err.message)
    })
})

// subscribing to auth changes
// onAuthStateChanged() returns a function that can be invoked to remove the
// monitoring of authorization changes from one particular browser instance
const unsubAuth = onAuthStateChanged(auth, (user) => {
  console.log('user status changed:', user)
})

// unsubscribing from changes (auth & db)
const unsubButton = document.querySelector('.unsub')
unsubButton.addEventListener('click', () => {
  console.log('unsubscribing')
  unsubCol()
  unsubDoc()
  unsubAuth()
})
